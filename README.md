# CMS Exploration - Drupal

Short-term project for Drupal CMS exploration on local (or hosted) environments. Uses Docker to build and deploy service.  

## Getting Started (Quick)
#### Prerequsites
* Docker is installed
* Ports 8080 and 8081 are available on your local workstation

### Command Line
```sh
$ git clone git@gitlab.oit.duke.edu:dul-its/cms-explore-drupal.git
$ docker-compose up -d # or sudo docker-compose up -d
$ docker ps # or sudo docker ps
```
Afterwards, **Drupal** will be running on port 8080,  **Wordpress** will be running on port 8081, and 
**MariaDB** will be running locally on port 4306 (*to avoid potential conflicts with port 3306*).

### Web Brower -- Drupal
Navigate to [localhost:8080](http://localhost:8080), and complete the Drupal setup form.  
  
#### For the Database section, use:  
**Database type**: MySQL, MariaDB...  
**Database name**: cms_explore  
**Database user**: cms_explore  
**Database password**: s3cretS4uce  
  
Under ***Advanced Options***:  
**Host**: db_explore  
**Port number**: 3306  
  
  
Press (or tap) "Save and continue"  

### Web Browser -- Wordpress
Navigate to [localhost:8081](localhost:8081) and complete the Wordpress setup process.  
***NOTE:*** The Wordpress container has the database setttings, so you will not have to specify them during setup.  
  
## Notes
#### Changing Ports
The ports can be changed in the `docker-compose.yml` file, in the event ports 8080 and/or 8081 are needed for other services on your workstation.  
  
#### Drupal & Drush
Drupal's "shell" application, ***Drush***, is included with the Drupal container. It can be accessed in a manner similar to this:  
  
**From your local shell:**
```sh
$ sudo docker ps  # do this to find the name of the running Drupal container
$ sudo docker exec -it <name-of-drupal-container> /bin/bash
root@container-hash: drush <command here>
```